using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Asteroides : MonoBehaviour
{
    
    public int hp;
    public int rapidez;
    public Rigidbody rbA;
    public Transform trA;
    Vector3 posicionAleatoria;
    /*public string _nombre;
    public int _nivelDeVida;
    public string nombre
    {
        get { return _nombre; }
        set { _nombre = value; }
    }
    public int nivelDeVida
    {
        get { return _nivelDeVida; }
        set { _nivelDeVida = value; }
    }

    public override void Movimiento()
    {

    }
   */
    public void Start()
    {
        rbA = GetComponent<Rigidbody>();
        trA = GetComponent<Transform>();
        generarRnd();

    }
    public void Update()
    {
        transform.LookAt(posicionAleatoria);
        transform.Translate(rapidez * Vector3.forward * Time.deltaTime);

    }
    public void recibirDaño()
    {
        hp = hp - 25;
        if (hp <= 0) { this.desaparecer(); }
    }
    private void desaparecer()
    { 
        Destroy(gameObject);
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("proyectil"))
        {
            recibirDaño();
            Destroy(gameObject);
        }
    }

    public void generarRnd()
    {
        float posX = Random.Range(-17f, 17f);
        float posZ = Random.Range(-10f, 10f);
        posicionAleatoria = new Vector3 (posX, 2f, posZ);
    }


}
