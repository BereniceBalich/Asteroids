using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Enemigos : IElementos
{
    public string Nombre { get; set; }
    public int NivelDeVida { get; set; }
    public abstract void Movimiento();
}
