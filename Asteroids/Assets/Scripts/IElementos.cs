
using UnityEngine;

public interface IElementos
{
    string Nombre { get; set; }
    int NivelDeVida { get; set; }
}
