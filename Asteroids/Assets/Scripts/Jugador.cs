using System.Collections;
using System.Collections.Generic;
using JetBrains.Annotations;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.InputSystem;
public class Jugador : MonoBehaviour
{
    public GameObject proyectil;
    public Transform trProyectil;
    public GameObject nave;
    public Rigidbody rb;
    public Transform tr;
    public float velocidad = 5f;
    public float velocidadRotacion = 100f;
    public int rapidez;
    public int rapidezDeBala;
    Vector3 objetivo;
    GameObject nuevoProyectil;
    public void Start()
    {
        rb = GetComponent<Rigidbody>();
        tr = nave.GetComponent<Transform>();
      
     

    }
    public void SetConstraits()
    {
        rb.constraints = RigidbodyConstraints.FreezePositionY;
    }

    public void movimientoNave()
    {
        float rotacion = 0f;
        Vector2 movimientoInput = Vector2.zero;

        if (Keyboard.current != null)
        {
            if (Keyboard.current.upArrowKey.isPressed)
            {
                movimientoInput += Vector2.up;

            }
            if (Keyboard.current.downArrowKey.isPressed)
            {
                movimientoInput += Vector2.down;
            }

            Vector3 movimiento = new Vector3(movimientoInput.x, 0f, movimientoInput.y) * velocidad * Time.deltaTime;
            transform.Translate(movimiento);

            if (Keyboard.current != null)
            {
                if (Keyboard.current.leftArrowKey.isPressed)
                {
                    rotacion += velocidadRotacion * Time.deltaTime;
                }
                if (Keyboard.current.rightArrowKey.isPressed)
                {
                    rotacion -= velocidadRotacion * Time.deltaTime;
                }
            }

            transform.Rotate(0f, rotacion, 0f);
        }
    }

    public void Disparar()
    {
        
        objetivo =  nave.transform.position + new Vector3(0f,0f,40f);
        if(Keyboard.current.spaceKey.isPressed)
        {
            proyectil.transform.position = nave.transform.position+ new Vector3(0f,0f,10f);
            proyectil.transform.LookAt(objetivo);
            proyectil.transform.Translate(rapidezDeBala * Vector3.forward * Time.deltaTime);
        }

    }
    private void OnCollisionEnter(Collision collision)
    {
       /* if (collision.gameObject.tag != "nave")
        {
            Destroy(proyectil);
        }*/
    }
}
